const Discord = require("discord.js");
const lyricsFinder = require("lyrics-finder");

//trusted
function trusted(x) {
    var DotJson = require('dot-json');
    var database = new DotJson('../blocked.json');
    var trustedMembers =  database.get('trusted');
    for (y = 0; y < trustedMembers.length; y++) {
        if (trustedMembers[y] == x) {return true}
    } return false
}

module.exports = {
    name: "lyrics",
    aliases: ["ly"],
    category: 'Music',
    utilisation: '{prefix}lyrics',

    async execute(client, message) {

        //geen muziek?
        let  EmbedNoSongPlaying = new Discord.MessageEmbed()
            .setColor('#fc9303')
            .setTitle(`Er speelt geen muziek`)
            .setDescription(`Er speelt nog geen muziek. Voeg liedjes toe met
                **/play [nummer]**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (!client.player.getQueue(message)) return message.channel.send(EmbedNoSongPlaying);

        //getlyrics
        const userID = message.author.id;
        const title = client.player.nowPlaying(message).title;
        const lyrics = await lyricsFinder(client.player.nowPlaying(message).title) || "Sorry, ik heb geen songtekst kunnen vinden voor je liedje :cry:";
        let lyricsPart = lyrics.split('\n\n');
        console.log(lyricsPart);
        let lyricsLength = lyricsPart.length;
        let part = 0;
     

        //lyrics embed
        let noLyricsFound = new Discord.MessageEmbed()
            .setTitle(`Lyrics van ${title}`)
            .setURL(`${client.player.nowPlaying(message).url}`)
            .setDescription(`${lyricsPart[part]}`)
            .setColor("#F8AA2A")
            .setTimestamp();
        if(lyrics == "Sorry, ik heb geen songtekst kunnen vinden voor je liedje :cry:") return message.channel.send(noLyricsFound);

        let lyricsEmbed = new Discord.MessageEmbed()
            .setTitle(`Lyrics van ${title}`)
            .setURL(`${client.player.nowPlaying(message).url}`)
            .setDescription(`${lyricsPart[part]}\n\n${part+1}/${lyricsLength}`)
            .setColor("#F8AA2A")
            .setTimestamp();
        
        message.channel.send(lyricsEmbed)
            .then(message => {
                return reactionColector(message);
            })

        //check for reactions
        async function reactionColector(message) {
            try {
                await message.react("⬅").then(() => {
                    message.react("➡")
                });
            } catch (err) {
                console.error(err);
                return;
            }

            const collector = await message.createReactionCollector((reaction, user) => !user.bot, {
                dispose: true,
            });

            collector.on("collect", (reaction, user) => {
                reaction.users.remove(user.id);
                if (trusted(user.id) || user.id === userID) {
                    if (reaction.emoji.name == "⬅") {
                        message.react("⬅").then(() => {
                            message.react("➡")
                        });
                        if (part != 0) {
                            part -= 1
                            let lyricsEmbed = new Discord.MessageEmbed()
                                .setTitle(`Lyrics van ${title}`)
                                .setURL(`${client.player.nowPlaying(message).url}`)
                                .setDescription(`${lyricsPart[part]}\n\n${part+1}/${lyricsLength}`)
                                .setColor("#F8AA2A")
                                .setTimestamp();
                            message.edit(lyricsEmbed)
                        }
                    }
                    if (reaction.emoji.name == "➡") {
                        message.react("⬅").then(() => {
                            message.react("➡")
                        });
                        if (part < lyricsLength - 1) {
                            part += 1
                            let lyricsEmbed = new Discord.MessageEmbed()
                                .setTitle(`Lyrics van ${title}`)
                                .setURL(`${client.player.nowPlaying(message).url}`)
                                .setDescription(`${lyricsPart[part]}\n\n${part+1}/${lyricsLength}`)
                                .setColor("#F8AA2A")
                                .setTimestamp();
                            message.edit(lyricsEmbed)
                        }
                    }
                }

            });
        }
    }
};